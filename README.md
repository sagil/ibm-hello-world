# IBM Task - hello world application deployed to Kubernetes  

Git repository: https://bitbucket.org/sagil/ibm-hello-world

This is a containerized Go web server for testing purposes. It is deployed to Google Kubernetes Engine (GKE) on Google Cloud Platform (GCP).
The web app itself does not do much, just returns the following string when calling GET on the root path:

    IBM Hello world app deployed to Kubernetes
    Version: 2.0.2
    Hostname: hello-app-6bb85c8544-mjr9z

This directory contains:
- `main.go` contains the HTTP server implementation. It responds to all HTTP
  requests with a  `Hello, world!` response.
- `Dockerfile` is used to build the Docker image for the application.
- `kubernetes/deployment.yaml` automated build file 

This app is based on Google's Kubernetes tutorial 
[Deploying a containerized web application](https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app)
and [Setting up automated deployments](https://cloud.google.com/kubernetes-engine/docs/how-to/automated-deployment)
 

# Automated build
* The Cloud Build trigger builds a container image after every push to the master branch of the Bitbucket repository.
* Cloud Build deploys a new revision of the workload to the cluster where the original image was deployed.

# Service Endpoint   
http://104.154.231.167/

Due to costs of the Kubernetes cluster on GCP the cluster may be shut down soon. 

# Screenshots 

Clusters

![clusters](images/clusters.png)

Services and ingress

![services and ingress](images/services_and_ingress.png)

Node pool details

![node pool](images/node_pool.png)

Endpoint app

![endpoint](images/endpoint.png)
